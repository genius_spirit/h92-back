const express = require("express");
const cors = require("cors");
const app = express();
const expressWs = require("express-ws")(app);

const port = 8000;

app.use(cors());

const clients = {};

app.ws("/pixels", (ws, req) => {
  const id = req.get("sec-websocket-key");
  clients[id] = ws;
  console.log("client connected");
  console.log("active connections: ", Object.values(clients).length);

  ws.on("message", msg => {

    let canvas;

    try {
      canvas = JSON.parse(msg);
    } catch (e) {
      ws.send({
        type: "ERROR",
        message: e
      });
    }

    switch (canvas.type) {
      case "CREATE_PIXELS":
        Object.values(clients).forEach(client => {
          client.send(JSON.stringify({
            type: "NEW_PIXELS_ARRAY",
            array: canvas.array
          }));
        });
        break;
      default:
        return ws.send(
          JSON.stringify({
            type: "ERROR",
            message: "Unknown message type"
          })
        );
    }
  });

  ws.on("close", () => {
    delete clients[id];
    console.log("client disconnected");
    console.log("active connections: ", Object.values(clients).length);
  });
});

app.listen(port, () => {
  console.log(`Server started on ${port} port!`);
});
